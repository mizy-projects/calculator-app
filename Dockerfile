FROM openjdk:17-jdk-slim

ARG APP_VERSION
ADD build/libs/calculator-${APP_VERSION}.jar /application.jar

EXPOSE 8080

CMD ["/application.jar"]
