# Simple Calculator

### An example calculus query:

**Original query:**

    2 * (23/(3*3))- 23 * (2*3)

**With encoding:**

    MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk=
 
### API Description:

**Endpoint:** 
    GET /calculus?query=[input]

The input can be expected to be UTF-8 with BASE64 encoding
 
**Return:**

On success: JSON response of format:

    { "error": false, "result": number }

On error: Either a HTTP error code or:

    { "error": true, "message": string }

**Supported operations:** 
    + - * / ( )

**Supported notations:**
- Negative numbers in brackets: `(-1)`
- Decimal separator `.` -> `1.1`

