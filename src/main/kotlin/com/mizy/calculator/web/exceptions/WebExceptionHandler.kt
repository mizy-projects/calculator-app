package com.mizy.calculator.web.exceptions

import com.mizy.calculator.app.exceptions.AppException
import com.mizy.calculator.web.ResponseDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.util.*

@ControllerAdvice
internal class WebExceptionHandler {

    @ExceptionHandler(RuntimeException::class)
    protected fun handleRuntimeException(appException: RuntimeException): ResponseEntity<ResponseDto> {
        return ResponseEntity<ResponseDto>(
            ResponseDto(error = true, message = appException.message),
            HttpStatus.INTERNAL_SERVER_ERROR
        )
    }

    @ExceptionHandler(AppException::class)
    protected fun handleAppException(appException: AppException): ResponseEntity<ResponseDto> {
        val webExceptionDescription: AppExceptionDetails =
            appException.javaClass.getDeclaredAnnotation(AppExceptionDetails::class.java)
        Objects.requireNonNull<Any>(webExceptionDescription)
        return ResponseEntity<ResponseDto>(
            ResponseDto(error = true, message = appException.message),
            webExceptionDescription.httpStatus
        )
    }
}