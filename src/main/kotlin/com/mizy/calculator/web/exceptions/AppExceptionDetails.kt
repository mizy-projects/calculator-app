package com.mizy.calculator.web.exceptions

import org.springframework.http.HttpStatus

@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class AppExceptionDetails(val httpStatus: HttpStatus)