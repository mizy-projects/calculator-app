package com.mizy.calculator.web

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ResponseDto(val error: Boolean, var result: String? = null, var message: String? = null)
