package com.mizy.calculator.web

import com.mizy.calculator.app.CalculusService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/calculus")
class CalculusController(val calculusService: CalculusService) {

    @GetMapping
    fun calculateQuery(@RequestParam(name = "query", required = true) query: String): ResponseEntity<ResponseDto> {
        val result = calculusService.calculateQuery(query)
        return ResponseEntity.ok(ResponseDto(error = false, result = result.toString()))
    }
}