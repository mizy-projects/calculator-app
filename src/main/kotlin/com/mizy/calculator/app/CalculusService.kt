package com.mizy.calculator.app

import com.mizy.calculator.app.Calculator.Companion.ALLOWED_TOKENS_REGEX
import com.mizy.calculator.app.exceptions.BadQueryException
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.*

@Service
class CalculusService(val calculator: Calculator) {

    fun calculateQuery(encodedQuery: String): BigDecimal {
        val decodedQuery = decodeQuery(encodedQuery)
        validateQuery(decodedQuery)
        return calculator.processQuery(decodedQuery)
    }

    private fun decodeQuery(query: String): String {
        try {
            val queryBytes = query.toByteArray(CHARSET)
            return Base64.getDecoder().decode(queryBytes).toString(CHARSET)
        } catch (e: IllegalArgumentException) {
            throw BadQueryException("invalid base64 format")
        }
    }

    private fun validateQuery(decodedQuery: String) {
        val query = decodedQuery.removeWhitespaces()
        if (query.isBlank())
            throw BadQueryException("empty query")

        val unexpectedTokens = query.replace(ALLOWED_TOKENS_REGEX.toRegex(), "")
        if (unexpectedTokens.isNotEmpty())
            throw BadQueryException("unexpectedTokens [$unexpectedTokens]")
    }

    companion object {
        val CHARSET = Charsets.UTF_8
    }

    fun String.removeWhitespaces(): String {
        return this.filter { !it.isWhitespace() }
    }
}
