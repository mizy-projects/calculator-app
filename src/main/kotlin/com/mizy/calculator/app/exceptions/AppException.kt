package com.mizy.calculator.app.exceptions

abstract class AppException(message: String) : RuntimeException(message)