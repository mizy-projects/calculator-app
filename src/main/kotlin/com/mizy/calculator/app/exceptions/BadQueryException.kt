package com.mizy.calculator.app.exceptions

import com.mizy.calculator.web.exceptions.AppExceptionDetails
import org.springframework.http.HttpStatus

@AppExceptionDetails(httpStatus = HttpStatus.BAD_REQUEST)
class BadQueryException(details: String) : AppException("Bad query format - $details")
