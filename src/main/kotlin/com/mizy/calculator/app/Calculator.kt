package com.mizy.calculator.app

import com.mizy.calculator.app.exceptions.BadQueryException
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

@Component
class Calculator {

    fun processQuery(query: String): BigDecimal {
        return parse(query).toBigDecimal()
    }

    private fun parse(query: String): String {
        val deque: Deque<String> = LinkedList()

        val accumulator = mutableListOf<Char>()
        query.forEach {
            if (it.isDigit() || it == DECIMAL_SEPARATOR) {
                accumulator.add(it)
            } else {
                clearAccumulator(accumulator, deque)

                if (it == RIGHT_BRACKET[0]) {
                    deque.add(unfoldBrackets(deque))
                } else {
                    deque.add(it.toString())
                }
            }
        }
        clearAccumulator(accumulator, deque)

        return interpret(deque.toTypedArray())
    }

    private fun clearAccumulator(accumulator: MutableList<Char>, deque: Deque<String>) {
        if (accumulator.isNotEmpty()) {
            deque.add(accumulator.joinToString(EMPTY_SEPARATOR))
            accumulator.clear()
        }
    }

    private fun unfoldBrackets(deque: Deque<String>): String {
        val fields = mutableListOf<String>()

        for (i in 0 until deque.size) {
            val field = deque.removeLast()
            if (field == LEFT_BRACKET) break

            fields.add(field)
        }

        return interpret(fields.reversed().toTypedArray())
    }

    private fun interpret(fields: Array<String>): String {
        val isNegativeNumber = fields.size == 2 && fields.first() == SUBTRACTION
        if (isNegativeNumber) return fields.joinToString(EMPTY_SEPARATOR)

        validateNumericFields(fields)

        var firstOperationIdx = fields.indexOfFirst { MULTIPLICATION == it || DIVISION == it }
        if (noIndexFound(firstOperationIdx)) {
            firstOperationIdx = fields.indexOfFirst { ADDITION == it || SUBTRACTION == it }
        }
        if (noIndexFound(firstOperationIdx)) {
            if (fields.size > 1) {
                throw BadQueryException("misplaced operators")
            } else {
                return fields.first()
            }
        }

        val leftTermIdx = firstOperationIdx - 1
        val prefixList = fields.copyOfRange(0, leftTermIdx)

        val rightTermIdx = firstOperationIdx + 2
        val suffixList = fields.copyOfRange(rightTermIdx, fields.size)

        val reducedFields = prefixList + processQuery(fields.copyOfRange(leftTermIdx, rightTermIdx)) + suffixList
        return interpret(reducedFields)
    }

    private fun validateNumericFields(fields: Array<String>) {
        val hasProperFields = !fields.first().isNumeric() || !fields.last().isNumeric()
        if (hasProperFields) {
            throw BadQueryException("misplaced operators")
        }
    }

    private fun noIndexFound(firstOperationIdx: Int): Boolean {
        return firstOperationIdx == -1
    }

    private fun processQuery(fields: Array<String>): String {
        validateNumericFields(fields)

        if (fields.size != VALID_FIELDS_SIZE) {
            throw IllegalStateException("Illegal calculation state - too many fields in inner calculation")
        }

        val operator = fields[1]
        val function = OPERATIONS[operator]

        if (function != null) {
            val leftTerm = fields[0]
            val rightTerm = fields[2]

            return function(leftTerm, rightTerm)
        } else {
            throw IllegalStateException("No operator function found")
        }
    }

    private fun String.isNumeric(): Boolean {
        return this.toDoubleOrNull() != null
    }

    companion object {
        private const val VALID_FIELDS_SIZE = 3
        private const val DIVISION_SCALE = 3

        private const val EMPTY_SEPARATOR = ""

        const val DECIMAL_SEPARATOR = '.'
        const val LEFT_BRACKET = "("
        const val RIGHT_BRACKET = ")"
        const val ADDITION = "+"
        const val SUBTRACTION = "-"
        const val MULTIPLICATION = "*"
        const val DIVISION = "/"
        const val ALLOWED_TOKENS_REGEX = "[0-9.\\-+*/()]"

        val OPERATIONS: Map<String, (String, String) -> String> = mapOf(
            ADDITION to { left, right -> (left.toBigDecimal() + right.toBigDecimal()).toString() },
            SUBTRACTION to { left, right -> (left.toBigDecimal() - right.toBigDecimal()).toString() },
            MULTIPLICATION to { left, right -> (left.toBigDecimal() * right.toBigDecimal()).toString() },
            DIVISION to { left, right ->
                left.toBigDecimal().divide(right.toBigDecimal(), DIVISION_SCALE, RoundingMode.HALF_UP).toString()
            }
        )
    }
}