package com.mizy.calculator.app


import com.mizy.calculator.app.exceptions.BadQueryException
import spock.lang.Specification

class CalculatorTest extends Specification {

    def "should calculate given query=[#query]"() {
        given:
        def calculator = new Calculator()

        when:
        def result = calculator.processQuery(query)

        then:
        result == expectedResult

        where:
        query                   | expectedResult
        "123"                   | 123
        "2+2"                   | 4.0
        "2+(2*3)"               | 8.0
        "2*(2*3)"               | 12.0
        "2*(2*3/(2*3))"         | 2.0
        "2*(23/(3*3))-23*(2*3)" | -132.888
        "2*(-2)"                | -4.0
        "(-2)*(-2)"             | 4.0
        "2*(-22)"               | -44.0
    }

    def "should throw [#expectedException] on invalid query=[#query]"() {
        given:
        def calculator = new Calculator()

        when:
        calculator.processQuery(query)

        then:
        thrown(expectedException)

        where:
        query    | expectedException
        "-2+2"   | BadQueryException.class
        "(-2+2)" | BadQueryException.class
        "2*+2"   | BadQueryException.class
    }
}
