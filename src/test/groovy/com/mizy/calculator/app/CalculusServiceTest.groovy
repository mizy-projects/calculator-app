package com.mizy.calculator.app

import com.mizy.calculator.app.exceptions.BadQueryException
import spock.lang.Specification

class CalculusServiceTest extends Specification {

    def "should decode base64 correct query=[#query]"() {
        given:
        def calculusService = new CalculusService(new Calculator())

        when:
        def result = calculusService.decodeQuery(query)

        then:
        result == expectedResult

        where:
        query                                  | expectedResult
        "MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk=" | "2 * (23/(3*3))- 23 * (2*3)"
        "IA=="                                 | " "
    }

    def "should fail to decode base64 incorrect query=[#query]"() {
        given:
        def calculusService = new CalculusService(new Calculator())

        when:
        calculusService.decodeQuery(query)

        then:
        thrown(Throwable.class)

        where:
        query << ["totally not base64"]
    }

    def "should validate valid query=[#query]"() {
        given:
        def calculusService = new CalculusService(new Calculator())

        when:
        calculusService.validateQuery(query)

        then:
        notThrown(Throwable.class)

        where:
        query << ["2 * (23/(3*3))- 23 * (2*3)", "2+2", "1       +    1"]
    }

    def "should throw BadQueryException on invalid query=[#query]"() {
        given:
        def calculusService = new CalculusService(new Calculator())

        when:
        calculusService.validateQuery(query)

        then:
        thrown(BadQueryException.class)

        where:
        query << [" ", "=!@#", "ABCD"]
    }
}
