package com.mizy.calculator.web


import org.springframework.http.HttpStatus

import java.net.http.HttpRequest
import java.net.http.HttpResponse

class CalculusControllerTest extends E2ETest {

    def "should return proper result for query=[#query]"() {
        given:
        def uri = "${appUrl()}/calculus?query=$query"

        when:
        def response = httpClient().send(HttpRequest.newBuilder()
                .uri(new URI(uri.toString()))
                .GET()
                .build(), HttpResponse.BodyHandlers.ofString())

        then:
        response.statusCode() == HttpStatus.OK.value()

        def responseObject = jsonSlurper.parseText(response.body())
        responseObject.error == false
        responseObject.result == expectedResult

        where:
        query                             | expectedResult
        asBase64("2*(23/(3*3))-23*(2*3)") | "-132.888"
        asBase64("2*(-22)")               | "-44"
        asBase64("2+2")                   | "4"
    }

    def "should return error for invalid query=[#query]"() {
        given:
        def uri = "${appUrl()}/calculus?query=$query"

        when:
        def response = httpClient().send(HttpRequest.newBuilder()
                .uri(new URI(uri.toString()))
                .GET()
                .build(), HttpResponse.BodyHandlers.ofString())

        then:
        response.statusCode() == expectedStatus.value()

        def responseObject = jsonSlurper.parseText(response.body())
        responseObject.error
        responseObject.message == expectedMessage

        where:
        query               | expectedMessage                             | expectedStatus
        ""                  | "Bad query format - empty query"            | HttpStatus.BAD_REQUEST
        "-2+2"              | "Bad query format - invalid base64 format"  | HttpStatus.BAD_REQUEST
        asBase64("")        | "Bad query format - empty query"            | HttpStatus.BAD_REQUEST
        asBase64("-!@#2+2") | "Bad query format - unexpectedTokens [!@#]" | HttpStatus.BAD_REQUEST
        asBase64("(-2+2)")  | "Bad query format - misplaced operators"    | HttpStatus.BAD_REQUEST
        asBase64("2*+2")    | "Bad query format - misplaced operators"    | HttpStatus.BAD_REQUEST
    }

    def asBase64(String query) {
        return Base64.getEncoder().encodeToString(query.getBytes())
    }
}
