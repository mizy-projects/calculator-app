package com.mizy.calculator.web


import groovy.json.JsonSlurper
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import java.net.http.HttpClient

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration
abstract class E2ETest extends Specification {

    @LocalServerPort
    private int port

    JsonSlurper jsonSlurper = new JsonSlurper()

    String appUrl() {
        return "http://localhost:$port/calculator-app"
    }

    HttpClient httpClient() {
        return HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .build()
    }
}

